const models = require('../models');

function getReading(req, res) {
    const {serialNumber, customerId} = req.query;

    models.meter.find({
        where: {customerId, serialNumber},
        include: [{model: models.reading}, {model: models.customer}]
    }).then(s => {
        if (s) {
            res.send(parseReading(s.dataValues));
        } else {
            res.status(204).send();
        }
    });
}

function parseReading(res) {
    return {
        'customerId': res.customerId,
        'serialNumber': res.serialNumber,
        'mpxn': res.mpxn,
        'read': res.readings.map(r => ({
            'type': r.type, 'value': r.value, 'readDate': r.readDate, 'registerId': r.registerId
        }))
    };
}

function createReading(req, res) {
    let params = req.body;

    // Deconstructing parameterr
    let customer = {customerId: params.customerId};
    let meter = {serialNumber: params.serialNumber, mpxn: params.mpxn,};
    let readings = params.read.map(r => Object.assign(r, {readDate: params.readDate}));

    models.customer.findOrCreate({where: {customerId: customer.customerId}})
        .then(s => models.meter.findOrCreate({
            where: {serialNumber: meter.serialNumber},
            defaults: Object.assign(meter, {customerId: s[0].dataValues.customerId})
        }))
        .then(s => {
            readings = readings.map(r => Object.assign(r, {meterId: s[0].dataValues.serialNumber}));
            return models.reading.bulkCreate(readings, {returning: true});
        })
        .then(s => res.send('Success'))
        .catch(console.log);
}

module.exports = {getReading, createReading};