const {check} = require('../validator');
const {getReading, createReading} = require('../controllers/readings');

module.exports = (app) => {
    app.route('/meter-read')
        .get(
            check('customerId', 'isString'),
            check('serialNumber', 'isNumber'),
            getReading
        )
        .post(
            check('customerId', 'isString'),
            check('serialNumber', 'isNumber'),
            check('mpxn', 'isNumber'),
            check('readDate', 'isDate'),
            check('read', 'isArrayType', {type: 'isString', registerId: 'isNumber', value: 'isNumber'}),
            createReading
        );
};