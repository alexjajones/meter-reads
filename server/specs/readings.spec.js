const expect = require('chai').expect;
const request = require('supertest');
const server = require('../index');
const deepcopy = require("deepcopy");

describe('Test creation of meter readings', () => {
    const valid = {
        "customerId": "identifier123",
        "serialNumber": "27263927192",
        "mpxn": "14582749",
        "read": [
            {"type": "ANYTIME", "registerId": "387373", "value": "2729"},
            {"type": "NIGHT", "registerId": "387373", "value": "2892"}
        ],
        "readDate": "2017-11-20T16:19:48+00:00"
    };

    it('Should accept valid params', done => {
        request(server)
            .post('/meter-read')
            .send(valid)
            .expect(200, done);
    });

    it('Should not accept empty read array', done => {
        const copy = deepcopy(valid);

        copy.read = [];

        request(server)
            .post('/meter-read')
            .send(copy)
            .expect(400, done);
    });

    describe('Should reject if ', () => {
        Object.keys(valid).forEach(k => {
            const copy = deepcopy(valid);

            delete copy[k];

            it(`${k} is missing.`, done => {
                request(server)
                    .post('/meter-read')
                    .send(copy)
                    .expect(400, done);
            });
        });
    });

    describe('Should reject if read is not valid', () => {
        Object.keys(valid.read[0]).forEach(k => {
            const copy = deepcopy(valid);

            delete copy.read[0][k];

            it(`${k} is missing from read.`, done => {
                request(server)
                    .post('/meter-read')
                    .send(copy)
                    .expect(400, done);
            });
        });
    });
});

describe('Test viewing of meter readings', () => {
    it('Should accept valid params', done => {
        request(server)
            .get('/meter-read?customerId=identifier123&serialNumber=27263927192')
            .expect(200, done);
    });
    it('Should accept valid params but not found', done => {
        request(server)
            .get('/meter-read?customerId=identifier1234&serialNumber=27263927192')
            .expect(204, done);
    });
    it('Should reject if no customerId is specified', done => {
        request(server)
            .get('/meter-read?serialNumber=27263927192')
            .expect(400, done);
    });
    it('Should reject if no serialNumber is specified', done => {
        request(server)
            .get('/meter-read?customerId=identifier1234')
            .expect(400, done);
    });
    it('Should reject if serialNumber is not number', done => {
        request(server)
            .get('/meter-read?customerId=identifier1234&serialNumber=abc27263927192')
            .expect(400, done);
    });
});