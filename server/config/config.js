module.exports = {
    development: {
        username: "alexjones",
        password: null,
        database: "meter-readings",
        host: "127.0.0.1",
        dialect: "postgres"
    },
    production: {
        username: process.env.db_username,
        password: process.env.db_password,
        database: process.env.db_name,
        host: process.env.db_host,
        dialect: 'postgres'
    }
};
