'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('meters', {
            id: {
                allowNull: false,
                autoIncrement: true,
                type: Sequelize.INTEGER
            },
            customerId: {
                allowNull: false,
                type: Sequelize.STRING,
                references: {model: 'customers', key: 'customerId'}
            },
            serialNumber: {
                allowNull: false,
                primaryKey: true,
                type: Sequelize.STRING,
            },
            mpxn: {
                allowNull: false,
                type: Sequelize.STRING
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('meters');
    }
};