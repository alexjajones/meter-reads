const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');

app.use(morgan('dev')); // log every request to the console

app.use(bodyParser.json()); // parse application/json

// test API
app.get('/hello', (req, res) => res.send('Hello World!!!!!!'));

const normalizedPath = require('path').join(__dirname, 'routes');

require('fs').readdirSync(normalizedPath).forEach((file) => {
    require('./routes/' + file)(app);
});

let server = app.listen(3000, () => console.log('--- Initialised meter reading service ---'));

module.exports = server;