const check = (prop, validationName, options) => (req, res, next) => {
    const params = req.method === 'GET' ? req.query : req.body;

    let validator = isPresentWrapper(validations[validationName]);

    if (!validationName) {
        return res.status(500).send(`Having troubles validating parameters.`);
    }

    let err = validator(params[prop], prop, options);

    if (err) {
        return res.status(400).send(`Invalid request, ${err}.`);
    }

    next();
};

const validations = {
    isNumber: (param, prop) => !isNaN(param) ? '' : `${prop} should be of type number`,
    isString: (param, prop) => typeof param === 'string' ? '' : `${prop} should be of type string`,
    isDate: (param, prop) => !isNaN(new Date(param).getTime()) ? '' : `${prop} should be of type date`,
    isArray: (param, prop) => param.constructor === Array && param.length ? '' : `${prop} should be of type none empty list`,
    isArrayType: (param, prop, options) => validations.isArray(param, prop) || validateArrayObject(param, options, prop)
};

/**
 * Iterates over array of objects and checks if properties exist on object and are of the correct type.
 * @param array - array of objects
 * @param spec - what the object should look like
 *               Example spec: {'username': 'isString', 'age': 'isNumber'}
 * @param prop - the higher order name for the array of objects
 * @returns an error string or nothing
 */

function validateArrayObject(array, spec, prop) {
    let keys = Object.keys(spec);

    for (let i = 0; i < array.length; i++) {
        let item = array[i];

        for (let j = 0; j < keys.length; j++) {
            let err;
            let key = keys[j];

            if (!item[key])
                err = `property ${key} should exist`;
            else
                err = validations[spec[key]](item[key], key);

            if (err) return `${err} on ${prop} entry ${i + 1} (interface: ${JSON.stringify(spec)})`;
        }
    }
}

function isPresentWrapper(func) {
    return (param, prop, options) => param ? func(param, prop, options) : `${prop} should exist`;
}

module.exports = {check};