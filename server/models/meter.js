'use strict';
module.exports = (sequelize, DataTypes) => {
    let meter = sequelize.define('meter', {
        serialNumber: {primaryKey: true, type: DataTypes.INTEGER},
        customerId: DataTypes.STRING,
        mpxn: DataTypes.INTEGER
    });

    meter.associate = function (models) {
        meter.belongsTo(models.customer, {foreignKey: 'customerId'});
        meter.hasMany(models.reading, {foreignKey: 'meterId', target: 'serialNumber'});
    };

    return meter;
};