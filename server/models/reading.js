'use strict';
module.exports = (sequelize, DataTypes) => {
    let reading = sequelize.define('reading', {
        type: DataTypes.STRING,
        registerId: DataTypes.INTEGER,
        meterId: DataTypes.INTEGER,
        value: DataTypes.INTEGER,
        readDate: DataTypes.DATE
    });

    reading.associate = function (models) {
        reading.belongsTo(models.meter, {foreignKey: 'meterId', target: 'serialNumber'});
    };

    return reading;
};