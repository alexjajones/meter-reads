'use strict';
module.exports = (sequelize, DataTypes) => {
    let customer = sequelize.define('customer', {
        customerId: {primaryKey: true, type: DataTypes.STRING}
    });

    customer.associate = function (models) {
        customer.hasOne(models.meter, {foreignKey: 'customerId'});
    };

    return customer;
};