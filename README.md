# Meter Readings
Service for managing meter readings via a Node API.

URL: http://34.239.167.195:3000/meter-read

# Features
* Simple and extendable Node based API.
* Intuitive parameter validation module.
* Fully unit tested.
* Solid database structure.
* Sequelize based models and migrations.
* Containerised (docker).
* CI (gitlab), building images and running tests on commit.
* Tests ran on CD mock the production environment, improving validity 
* CD (gitlab), one click deployment from master branch.
* Secure credential management for database credentials.
* Hosted on AWS using AWS Fargate (lambda for docker) and AWS RDS (Postgres).

# API Interface

## GET /meter-read?**customerId**=ID&**serialNumber**=NUMBER
Making a GET request passing the paramters `customerId` and `serialNumber` will return the readings that belong to a 
meter.

Example result: 
```json
{
    "customerId": "identifier123",
    "serialNumber": "27263927192",
    "mpxn": "14582749",
    "read": [
        {
            "type": "ANYTIME",
            "value": 2729,
            "readDate": "2017-11-20T16:19:48.000Z",
            "registerId": "387373"
        },
        {
            "type": "NIGHT",
            "value": 2892,
            "readDate": "2017-11-20T16:19:48.000Z",
            "registerId": "387373"
        }
    ]
}
```

## POST /meter-read
Make a post request will create assets if they don't exist (customer, meter and readings).

Example body: 

```json
{
    "customerId": "identifier123",
    "serialNumber": "27263927192",
    "mpxn": "14582749",
    "read": [
        {"type": "ANYTIME", "registerId": "387373", "value": "2729"},
        {"type": "NIGHT", "registerId": "387373", "value": "2892"}
    ],
    "readDate": "2017-11-20T16:19:48+00:00"
}
```

# Usage of server within development
## Setup
* Pull repo
* cd server
* Run `npm install`
* Create Postgres database to house data named meter-readings

## Starting
* cd server
* Run `npm start`

## Running unit tests
* cd server
* Run `npm test`